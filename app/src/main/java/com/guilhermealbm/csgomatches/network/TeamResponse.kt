package com.guilhermealbm.csgomatches.network

import com.google.gson.annotations.SerializedName

data class TeamResponse(
    val acronym: String?,
    val id: Int,
    @SerializedName("image_url") val imageUrl: String?,
    val location: String?,
    val name: String?,
    val players: List<PlayerResponse>?,
    val slug: String?
)

data class PlayerResponse(
    val first_name: String?,
    val id: Int,
    @SerializedName("image_url") val imageUrl: String?,
    val last_name: String?,
    val name: String?,
    val slug: String?
)