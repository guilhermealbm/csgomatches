package com.guilhermealbm.csgomatches.network

import com.google.gson.annotations.SerializedName

data class MatchResponse(
    val id: Int,
    val league: League?,
    val opponents: List<Opponent>,
    @SerializedName("scheduled_at") val scheduledAt: String?,
    val serie: Serie?,
    @SerializedName("serie_id") val serieId: Int?,
    val slug: String?,
    val status: String?,
)

data class League(
    val id: Int,
    val name: String,
    @SerializedName("image_url") val imageUrl: String?,
    val slug: String?
)

data class Opponent(
    val opponent: OpponentDetails,
    val type: String?
)

data class OpponentDetails(
    val acronym: String?,
    val id: Int?,
    @SerializedName("image_url") val imageUrl: String?,
    val location: String?,
    @SerializedName("modified_at") val modifiedAt: String?,
    val name: String?,
    val slug: String?
)

data class Serie(
    val id: Int,
    val name: String,
)
