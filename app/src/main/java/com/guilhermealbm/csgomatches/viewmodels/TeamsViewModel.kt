package com.guilhermealbm.csgomatches.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.guilhermealbm.csgomatches.domain.TeamWithPlayers
import com.guilhermealbm.csgomatches.usecases.GetTeamsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TeamsViewModel @Inject constructor(
    private val getTeamsUseCase: GetTeamsUseCase
) : ViewModel() {

    private val _uiState = MutableStateFlow<List<TeamWithPlayers?>>(emptyList())
    val uiState: StateFlow<List<TeamWithPlayers?>>
        get() = _uiState

    fun getTeams(team1Id: String, team2id: String) {
        viewModelScope.launch {
            _uiState.value = getTeamsUseCase(team1id = team1Id, team2id = team2id)
        }
    }

}