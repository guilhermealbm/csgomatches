package com.guilhermealbm.csgomatches.ui.player

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.guilhermealbm.csgomatches.R
import com.guilhermealbm.csgomatches.ui.theme.CsGoMatchesTheme

@Composable
fun PlayerView(
    nickname: String,
    name: String,
    photoUrl: String,
    isLeft: Boolean
) {
    Surface(
        modifier = Modifier.padding(
            start = if (isLeft) 0.dp else 6.dp,
            end = if (isLeft) 6.dp else 0.dp
        ).fillMaxWidth().height(54.dp),
        shape = RoundedCornerShape(
            topStart = if (isLeft) 0.dp else 12.dp,
            topEnd = if (isLeft) 12.dp else 0.dp,
            bottomStart = if (isLeft) 0.dp else 12.dp,
            bottomEnd = if (isLeft) 12.dp else 0.dp
        )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 8.dp),
            horizontalArrangement = if (isLeft) Arrangement.End else Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {

            if (!isLeft) {
                AsyncImage(
                    modifier = Modifier.padding(end = 16.dp).size(50.dp),
                    model = photoUrl,
                    contentDescription = "",
                    placeholder = painterResource(R.drawable.player_logo_placeholder),
                    error = painterResource(R.drawable.player_logo_placeholder)
                )
            }

            Column (
                horizontalAlignment = if (isLeft) Alignment.End else Alignment.Start
            ) {
                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    text = nickname,
                    style = MaterialTheme.typography.body1,
                    fontSize = 14.sp,
                    textAlign = if (isLeft) TextAlign.End else TextAlign.Start
                )
                Text(
                    text = name,
                    style = MaterialTheme.typography.body1,
                    fontSize = 12.sp,
                    textAlign = if (isLeft) TextAlign.End else TextAlign.Start,
                    color = Color(0xFF6C6B7E)
                )
            }
            if (isLeft) {
                AsyncImage(
                    modifier = Modifier.padding(start = 16.dp).size(50.dp),
                    model = photoUrl,
                    contentDescription = "",
                    placeholder = painterResource(R.drawable.player_logo_placeholder),
                    error = painterResource(R.drawable.player_logo_placeholder)
                )
            }
        }
    }
}

@Preview
@Composable
private fun PlayerViewPreview() {
    CsGoMatchesTheme {
        PlayerView(
            "Nickname",
            "Name",
            "",
            false
        )
    }
}