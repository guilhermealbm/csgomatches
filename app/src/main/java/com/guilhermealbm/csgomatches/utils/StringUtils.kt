package com.guilhermealbm.csgomatches.utils

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

fun toLeagueAndSeries(league: String, series: String) =
    if (series.isEmpty()) league else "$league - $series"

fun Date.getDayAndTime(dateFormat: SimpleDateFormat) = dateFormat.format(this).replace(".", "")
    .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

fun checkIsToday(date: Date, originalText: String, today: String) : String {
    val isTodayFormat = SimpleDateFormat("yyyy-MM-dd")
    val now = LocalDate.now()
    val localDate = LocalDate.parse(isTodayFormat.format(date))
    val isToday = now == localDate

    if (isToday) {
        val newDateFormat = SimpleDateFormat("HH:mm")
        return "$today ${newDateFormat.format(date)}"
    }
    return originalText
}