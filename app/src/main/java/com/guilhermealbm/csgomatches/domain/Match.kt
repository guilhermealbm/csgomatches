package com.guilhermealbm.csgomatches.domain

import java.util.Date


data class Match(
    val team1: Team,
    val team2: Team,
    val isLive: Boolean,
    val time: Date?,
    val league: String,
    val leagueUrl: String,
    val serie: String
)