package com.guilhermealbm.csgomatches.domain

data class TeamWithPlayers(
    val imageUrl: String,
    val name: String,
    val id: Int,
    val players: List<Player>?,
)
