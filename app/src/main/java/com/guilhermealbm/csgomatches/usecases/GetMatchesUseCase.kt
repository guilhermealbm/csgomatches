package com.guilhermealbm.csgomatches.usecases

import com.guilhermealbm.csgomatches.domain.Match
import com.guilhermealbm.csgomatches.repositories.MatchesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetMatchesUseCase @Inject constructor(
    private val matchesRepository: MatchesRepository
) {
    suspend operator fun invoke(dispatcher: CoroutineDispatcher = Dispatchers.Default) : List<Match> {
        return withContext(dispatcher) {
            matchesRepository.getMatches() + matchesRepository.getUpcomingMatches()
        }
    }
}