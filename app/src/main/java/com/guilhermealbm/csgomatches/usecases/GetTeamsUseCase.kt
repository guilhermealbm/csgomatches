package com.guilhermealbm.csgomatches.usecases

import com.guilhermealbm.csgomatches.domain.TeamWithPlayers
import com.guilhermealbm.csgomatches.repositories.TeamsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetTeamsUseCase @Inject constructor(
    private val teamsRepository: TeamsRepository
) {

    suspend operator fun invoke(
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
        team1id: String,
        team2id: String) : List<TeamWithPlayers?> {
        return withContext(dispatcher) {
            listOf(teamsRepository.getTeamById(team1id), teamsRepository.getTeamById(team2id))
        }
    }

}