package com.guilhermealbm.csgomatches.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.guilhermealbm.csgomatches.domain.Match
import com.guilhermealbm.csgomatches.usecases.GetMatchesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MatchesViewModel @Inject constructor(
    private val getMatchesUseCase: GetMatchesUseCase
) : ViewModel() {
    private val _uiState = MutableStateFlow<List<Match>>(emptyList())
    val uiState: StateFlow<List<Match>>
        get() = _uiState

    private val _isRefreshing = MutableStateFlow(false)
    val isRefreshing: StateFlow<Boolean>
        get() = _isRefreshing

    init {
        viewModelScope.launch {
            _uiState.value = getMatchesUseCase()
        }
    }

    fun refresh() {
        _isRefreshing.value = true
        viewModelScope.launch {
            _uiState.value = getMatchesUseCase()
            _isRefreshing.value = false
        }
    }
}