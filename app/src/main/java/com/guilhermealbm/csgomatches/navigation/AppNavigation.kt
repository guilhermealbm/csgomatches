package com.guilhermealbm.csgomatches.navigation

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.guilhermealbm.csgomatches.ui.matches.DetailScreen
import com.guilhermealbm.csgomatches.ui.matches.MatchesScreen
import com.guilhermealbm.csgomatches.ui.theme.CsGoMatchesTheme

@Composable
fun AppNavigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.MainScreen.route) {
        composable(route = Screen.MainScreen.route) {
            CsGoMatchesTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color(0xFF252525)
                ) {
                    MatchesScreen(navController = navController)
                }
            }
        }
        composable(
            route = Screen.DetailScreen.route + "/{title}/{team1id}/{team2id}/{date}",
            arguments = listOf(
                navArgument("title") {
                    type = NavType.StringType
                    defaultValue = ""
                },
                navArgument("team1id") {
                    type = NavType.StringType
                    defaultValue = ""
                },
                navArgument("team2id") {
                    type = NavType.StringType
                    defaultValue = ""
                },
                navArgument("date") {
                    type = NavType.StringType
                    defaultValue = ""
                }
            )
        ) {
            CsGoMatchesTheme {
                DetailScreen(
                    title = it.arguments?.getString("title"),
                    team1id = it.arguments?.getString("team1id"),
                    team2id = it.arguments?.getString("team2id"),
                    date = it.arguments?.getString("date"),
                    onClick = { navController.navigateUp() }
                )
            }
        }
    }
}
