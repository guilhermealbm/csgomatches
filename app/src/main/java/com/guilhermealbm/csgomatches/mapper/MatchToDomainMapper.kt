package com.guilhermealbm.csgomatches.mapper

import com.guilhermealbm.csgomatches.domain.Match
import com.guilhermealbm.csgomatches.domain.Player
import com.guilhermealbm.csgomatches.domain.Team
import com.guilhermealbm.csgomatches.domain.TeamWithPlayers
import com.guilhermealbm.csgomatches.network.MatchResponse
import com.guilhermealbm.csgomatches.network.Opponent
import com.guilhermealbm.csgomatches.network.PlayerResponse
import com.guilhermealbm.csgomatches.network.TeamResponse
import java.text.SimpleDateFormat

fun MatchResponse.toMatch() : Match? {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    return if (opponents.size == 2) {
        Match(
            team1 = opponents[0].toTeam(),
            team2 = opponents[1].toTeam(),
            isLive = status == MapConstants.IS_LIVE,
            time = scheduledAt?.let { dateFormat.parse(it) },
            league = league?.name.orEmpty(),
            leagueUrl = league?.imageUrl.orEmpty(),
            serie = serie?.name.orEmpty()
        )
    } else {
        null
    }
}

fun Opponent.toTeam() = Team(
    id = opponent.id.toString(),
    imageUrl = opponent.imageUrl.orEmpty(),
    name = opponent.name.orEmpty()
)

fun TeamResponse.toTeamWithPlayers() = TeamWithPlayers(
    imageUrl = imageUrl.orEmpty(),
    name = name.orEmpty(),
    id = id,
    players = players?.map { it.toPlayer() },
)

fun PlayerResponse.toPlayer() = Player(
    id = id,
    firstName = first_name.orEmpty(),
    lastName = last_name.orEmpty(),
    nickName = name.orEmpty(),
    imageUrl = imageUrl.orEmpty()
)


object MapConstants {
    const val IS_LIVE = "running"
}