package com.guilhermealbm.csgomatches.ui.matches

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.PullRefreshState
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.guilhermealbm.csgomatches.R
import com.guilhermealbm.csgomatches.domain.Match
import com.guilhermealbm.csgomatches.navigation.Screen
import com.guilhermealbm.csgomatches.utils.checkIsToday
import com.guilhermealbm.csgomatches.utils.getDayAndTime
import com.guilhermealbm.csgomatches.utils.toLeagueAndSeries
import com.guilhermealbm.csgomatches.viewmodels.MatchesViewModel
import java.text.SimpleDateFormat

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun MatchesScreen(
    navController: NavController,
    viewModel: MatchesViewModel = hiltViewModel()
) {
    val uiState = viewModel.uiState.collectAsState()
    val isRefreshing = viewModel.isRefreshing.collectAsState()

    val pullRefreshState = rememberPullRefreshState(isRefreshing.value, { viewModel.refresh() })

    MatchesScreen(navController = navController, matches = uiState.value, isRefreshing = isRefreshing.value, pullRefreshState = pullRefreshState)
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun MatchesScreen(
    navController: NavController,
    matches: List<Match>,
    isRefreshing: Boolean,
    pullRefreshState: PullRefreshState
) {
    Column(modifier = Modifier.padding(start = 24.dp, end = 24.dp, top = 24.dp)) {
        Text(
            text = stringResource(id = R.string.matches),
            style = MaterialTheme.typography.h1
        )
        Spacer(modifier = Modifier.size(24.dp))
        Box(modifier = Modifier.pullRefresh(pullRefreshState)) {
            LazyColumn {
                items(matches) {

                    val finalText = it.time?.let { date ->
                        val originalText = date.getDayAndTime(SimpleDateFormat("EEE, HH:mm"))
                        return@let checkIsToday(date, originalText, stringResource(id = R.string.today))
                    }

                    MatchView(
                        team1Url = it.team1.imageUrl,
                        team1Name = it.team1.name,
                        team2Url = it.team2.imageUrl,
                        team2Name = it.team2.name,
                        isLive = it.isLive,
                        matchDate = it.time,
                        leagueAndSeries = toLeagueAndSeries(it.league, it.serie),
                        leagueUrl = it.leagueUrl,
                        onClick = { navController.navigate(Screen.DetailScreen.withArgs(
                            toLeagueAndSeries(it.league, it.serie), it.team1.id, it.team2.id, finalText.orEmpty()
                        )) }
                    )
                    Spacer(modifier = Modifier.size(24.dp))
                }
            }
            if (matches.isNotEmpty()) {
                PullRefreshIndicator(
                    refreshing = isRefreshing,
                    state = pullRefreshState,
                    modifier = Modifier.align(Alignment.TopCenter),
                    backgroundColor = Color(0xFF252525),
                    contentColor = Color.White
                )
            }
        }
        if (matches.isEmpty()) {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                CircularProgressIndicator(
                    modifier = Modifier.size(64.dp)
                )
            }
        }
    }
}