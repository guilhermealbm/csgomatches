package com.guilhermealbm.csgomatches.services

import com.guilhermealbm.csgomatches.BuildConfig
import com.guilhermealbm.csgomatches.network.MatchResponse
import com.guilhermealbm.csgomatches.network.TeamResponse
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface PandaScoreService {

    @GET("matches/running")
    suspend fun runningMatches() : List<MatchResponse>

    @GET("matches/upcoming")
    suspend fun upcomingMatches() : List<MatchResponse>

    @GET("teams/")
    suspend fun getTeams(
        @Query("filter[id]", encoded = true) id: String
    ) : List<TeamResponse>

    companion object {
        private const val BASE_URL = "https://api.pandascore.co/csgo/"

        fun create(): PandaScoreService {
            val logger = HttpLoggingInterceptor()
                .apply { level = Level.HEADERS }

            val interceptor = Interceptor {
                val request = it.request().newBuilder().addHeader("Authorization", "Bearer ${BuildConfig.API_ACCESS_TOKEN}").build()
                it.proceed(request)
            }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .addInterceptor(interceptor)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PandaScoreService::class.java)
        }
    }
}
