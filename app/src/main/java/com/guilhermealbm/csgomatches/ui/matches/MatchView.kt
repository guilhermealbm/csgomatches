package com.guilhermealbm.csgomatches.ui.matches

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.guilhermealbm.csgomatches.R
import com.guilhermealbm.csgomatches.utils.checkIsToday
import com.guilhermealbm.csgomatches.utils.getDayAndTime
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun MatchView(
    team1Url: String,
    team1Name: String,
    team2Url: String,
    team2Name: String,
    isLive: Boolean,
    matchDate: Date?,
    leagueAndSeries: String,
    leagueUrl: String,
    onClick: () -> Unit
) {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onClick()
            },
        shape = RoundedCornerShape(16.dp)
    ) {
        Column(modifier = Modifier
            .fillMaxWidth()
            .background(Color(0xFF272639))
        ) {

            Row {
                Spacer(Modifier.weight(1f))
                if (isLive) {
                    LiveOrDateFlag(
                        backgroundColor = Color(0xFFF42A35),
                        text = stringResource(id = R.string.liveNow)
                    )
                } else {
                    matchDate?.let { date ->
                        val originalText = date.getDayAndTime(SimpleDateFormat("EEE, HH:mm"))
                        val finalText = checkIsToday(date, originalText, stringResource(id = R.string.today))

                        LiveOrDateFlag(
                            backgroundColor = Color(0x33FAFAFA),
                            text = finalText
                        )
                    }
                }
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                TeamView(badgeUrl = team1Url, teamName = team1Name)
                Text(
                    modifier = Modifier.padding(horizontal = 12.dp),
                    text = stringResource(id = R.string.versus),
                    style = MaterialTheme.typography.h1,
                    fontSize = 12.sp,
                    color = Color(0x80FFFFFF)
                )
                TeamView(badgeUrl = team2Url, teamName = team2Name)
            }

            Divider(
                color = Color(0x33FFFFFF)
            )

            Row(
                modifier = Modifier.padding(start = 16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                if (leagueUrl.isNotEmpty()) {
                    AsyncImage(
                        modifier = Modifier.size(16.dp),
                        model = leagueUrl,
                        contentDescription = "",
                        placeholder = painterResource(R.drawable.team_logo_placeholder),
                        error = painterResource(R.drawable.team_logo_placeholder)
                    )
                }

                Text(
                    modifier = Modifier.padding(horizontal = 8.dp, vertical = 12.dp),
                    text = leagueAndSeries,
                    style = MaterialTheme.typography.body1,
                    fontSize = 8.sp
                )
            }

        }
    }
}

@Composable
fun LiveOrDateFlag(
    backgroundColor: Color,
    text: String
) {
    Surface(
        shape = RoundedCornerShape(
            topStart = 0.dp,
            topEnd = 16.dp,
            bottomStart = 16.dp,
            bottomEnd = 0.dp
        )
    ) {
        Column(
            modifier = Modifier
                .background(color = backgroundColor)
                .padding(8.dp)
        ) {
            Text(
                text = text,
                style = MaterialTheme.typography.body1,
                fontSize = 8.sp
            )
        }
    }
}