# CsGoMatches

This repository FuzeApp, an Android application that displays a list of CS: GO matches in a given period of time using PandaScore Api. It follows the MVVM architecture with Clean Architecture principles.

## Libraries
- Coil
- Retrofit
- Jetpack Libraries (Compose, Lifecycle, Hilt...)

## Installation and Running the Project

To run the project, follow these steps:

- Clone the repository;
- Open the project in Android Studio;
- Build and run the app on a device or emulator;
- Once the app is running, you can interact with it as you would with any other Android app.

## Screenshots

Matches             |  Match Detail
:-------------------------:|:-------------------------:
![](photo_2023-03-21_00.20.10.jpeg)  |  ![](photo_2023-03-21_00.20.13.jpeg)
