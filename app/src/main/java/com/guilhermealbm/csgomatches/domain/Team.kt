package com.guilhermealbm.csgomatches.domain

data class Team(
    val id: String,
    val imageUrl: String,
    val name: String
)
