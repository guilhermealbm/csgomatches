package com.guilhermealbm.csgomatches.ui.matches

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.guilhermealbm.csgomatches.R
import com.guilhermealbm.csgomatches.domain.TeamWithPlayers
import com.guilhermealbm.csgomatches.ui.player.PlayerView
import com.guilhermealbm.csgomatches.viewmodels.TeamsViewModel

@Composable
fun DetailScreen(
    title: String?,
    team1id: String?,
    team2id: String?,
    date: String?,
    onClick: () -> Unit,
    viewModel: TeamsViewModel = hiltViewModel()
) {
    val uiState = viewModel.uiState.collectAsState()
    LaunchedEffect(uiState) {
        if (uiState.value.isEmpty()) {
            viewModel.getTeams(team1id.orEmpty(), team2id.orEmpty())
        }
    }
    DetailScreen(title, date, uiState.value, onClick)
}

@Composable
fun DetailScreen(
    title: String?,
    date: String?,
    teams: List<TeamWithPlayers?>,
    onClick: () -> Unit,
) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color(0xFF252525)
    ) {
        Column(
            modifier = Modifier
                .padding(vertical = 24.dp)
                .fillMaxSize()
        ) {

            Toolbar(onClick = onClick, title = title.orEmpty())
            Spacer(modifier = Modifier.height(24.dp))

            if (teams.size == 2) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    TeamView(
                        badgeUrl = teams[0]?.imageUrl.orEmpty(),
                        teamName = teams[0]?.name.orEmpty()
                    )
                    Text(
                        modifier = Modifier.padding(horizontal = 12.dp),
                        text = stringResource(id = R.string.versus),
                        style = MaterialTheme.typography.h1,
                        fontSize = 12.sp,
                        color = Color(0x80FFFFFF)
                    )
                    TeamView(
                        badgeUrl = teams[1]?.imageUrl.orEmpty(),
                        teamName = teams[1]?.name.orEmpty()
                    )
                }

                Spacer(modifier = Modifier.height(20.dp))
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    date?.let {
                        Text(
                            text = it,
                            style = MaterialTheme.typography.body1,
                            fontWeight = FontWeight.Bold,
                            fontSize = 12.sp
                        )
                    }
                }
                Spacer(modifier = Modifier.height(24.dp))

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    LazyColumn(
                        modifier = Modifier.weight(1f)
                    ) {
                        items(teams[0]?.players!!) {
                            PlayerView(nickname = it.nickName, name = it.firstName, photoUrl = it.imageUrl, isLeft = true)
                            Spacer(modifier = Modifier.height(16.dp))
                        }
                    }
                    LazyColumn(
                        modifier = Modifier.weight(1f),
                        horizontalAlignment = Alignment.End,
                    ) {
                        items(teams[1]?.players!!) {
                            PlayerView(nickname = it.nickName, name = it.firstName, photoUrl = it.imageUrl, isLeft = false)
                            Spacer(modifier = Modifier.height(16.dp))
                        }
                    }
                }
            }


            if (teams.isEmpty()) {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator(
                        modifier = Modifier.size(64.dp)
                    )
                }
            }
        }
    }

}

@Composable
fun Toolbar(
    onClick: () -> Unit,
    title: String
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 24.dp),
        contentAlignment = Alignment.Center
    ) {
        IconButton(
            modifier = Modifier
                .align(Alignment.CenterStart)
                .size(24.dp),
            onClick = onClick
        ) {
            Icon(
                imageVector = Icons.Filled.ArrowBack,
                contentDescription = null,
                tint = Color.White
            )
        }

        Text(
            modifier = Modifier.padding(horizontal = 48.dp),
            text = title,
            style = MaterialTheme.typography.h1,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            fontSize = 18.sp
        )
    }
}