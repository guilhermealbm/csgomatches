package com.guilhermealbm.csgomatches.repositories

import com.guilhermealbm.csgomatches.domain.Match
import com.guilhermealbm.csgomatches.mapper.toMatch
import com.guilhermealbm.csgomatches.services.PandaScoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MatchesRepository @Inject constructor(private val service: PandaScoreService) {

    suspend fun getMatches(): List<Match> {
        return service.runningMatches().mapNotNull { it.toMatch() }
    }

    suspend fun getUpcomingMatches(): List<Match> {
        return service.upcomingMatches().mapNotNull { it.toMatch() }
    }

}