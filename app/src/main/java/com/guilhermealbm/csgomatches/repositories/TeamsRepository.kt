package com.guilhermealbm.csgomatches.repositories

import com.guilhermealbm.csgomatches.domain.TeamWithPlayers
import com.guilhermealbm.csgomatches.mapper.toTeamWithPlayers
import com.guilhermealbm.csgomatches.services.PandaScoreService
import javax.inject.Inject

class TeamsRepository @Inject constructor(private val service: PandaScoreService) {

    suspend fun getTeamById(id: String): TeamWithPlayers? {
        val team = service.getTeams(id).firstOrNull()
        return team?.toTeamWithPlayers()
    }

}