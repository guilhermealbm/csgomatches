package com.guilhermealbm.csgomatches.di

import com.guilhermealbm.csgomatches.services.PandaScoreService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun providePandaScoreService(): PandaScoreService {
        return PandaScoreService.create()
    }

}