package com.guilhermealbm.csgomatches.ui.matches

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.guilhermealbm.csgomatches.R

@Composable
fun TeamView(
    badgeUrl: String,
    teamName: String
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AsyncImage(
            modifier = Modifier.size(60.dp),
            model = badgeUrl,
            contentDescription = "",
            placeholder = painterResource(R.drawable.team_logo_placeholder),
            error = painterResource(R.drawable.team_logo_placeholder),
        )
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = teamName,
            style = MaterialTheme.typography.body1
        )
    }
}