package com.guilhermealbm.csgomatches.domain

data class Player(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val nickName: String,
    val imageUrl: String
)
